<p center="center">
    <h1>病歷整合查詢(護理)</h1>
</p>

<p align="center">
    <a href="https://vuejs.org/v2/guide/">
        <img src="https://img.shields.io/badge/vue-4.5.14-blue">
    </a>
    <a href="https://getbootstrap.com/docs/4.6/getting-started/introduction/">
        <img src="https://img.shields.io/badge/bootstrap-4.5.2-blue">
    </a>
</p>

## 簡介
病歷整合查詢是給急診護理部一套後台病歷查詢方案

## 功能

```textile
急診護理評估
急診護理紀錄
照會履歷
急診約束
急診意識評估
營養照顧紀錄
急診轉歸動向
急診生命徵象
麻醉前評估單
手術後轉 ICU 交班單
```

### 開發
```
#克隆項目
git clone https://gitlab.com/whiteHatMimic/oprw5000.git

#進入目錄
cd oprw5000

#安裝依賴
npm install

#啟動服務
npm serve
```

## 發佈
```git
#打包檔案
npm build
```