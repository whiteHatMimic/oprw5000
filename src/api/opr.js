import request from '@/utils/request'

/**
 * 取得急診護理評估
 * @param {string} hsp
 * @param {string} crtno
 * @param {string} actno
 */
export function getERNurEvaValue(hsp, crtno, actno){
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/opr211api/Get_ERNur_EvaValue',
        method: 'get',
        params: {
            'hsp': hsp,
            'crtno': crtno,
            'actno': actno
        }
    });
}


/**
 * 取得急診護理紀錄
 * @param {string} hspnme 院區
 * @param {string} crtno 病歷號
 * @param {string} actno 流水號
 */
export function getERNurRec(hspnme, crtno, actno){
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/opr211api/Get_ERNurRec',
        method: 'get',
        params: {
            'hspnme': hspnme,
            'crtno': crtno,
            'actno': actno
        }
    });
}


/**
 * 取得照會清單
 * @param {string} hspnme 院區
 * @param {string} crtno 病歷號
 * @param {string} crtseq 流水號
 */
export function getNoteResumeList(hspnme, crtno, crtseq) {
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nur2400api/GetNur2400List',
        method: 'get',
        params: {
            hspnme: hspnme,
            crtno: crtno,
            crtseq: crtseq
        }
    });
}


/**
 * 取得照會問題內容
 * @param {string} hspnme 院區
 * @param {string} crtno 病歷號
 * @param {string} crtseq 流水號
 * @param {string} evldte 照會日期
 * @param {string} dtype 照會版本
 */
export function getNoteResumeProblemContent(hspnme, crtno, crtseq, evldte, dtype) {
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nur2400api/Get_Meet_Ques',
        method: 'get',
        params: {
            hspnme: hspnme,
            crtno: crtno,
            crtseq: crtseq,
            evldte: evldte,
            dtype: dtype
        }
    });
}


/**
 * 取得照會回覆內容
 * @param {string} hsp 院區
 * @param {string} crtno 病歷號
 * @param {string} actno 流水號
 * @param {string} evldte 填寫日期
 * @param {string} dtype 照會類別
 * @param {string} version 照會版本
 * @param {string} srflg 來源
 */
export function getNoteResumeReplyContent(hsp, crtno, crtseq, evldte, dtype, version, srflg)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nur2400api/Get_Meet_Answer',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            crtseq: crtseq,
            evldte: evldte,
            dtype: dtype,
            version: version,
            srflg: srflg
        }
    });
}


/**
 * 取得急診約束紀錄清單
 * @param {string} hsp 院區
 * @param {string} crtno 病歷號
 * @param {string} actno 流水號
 * @param {string} opid 員編
 */
export function getOprTraintList(hsp, crtno, actno, opid)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nisp1000api/Get_ERNUR_TraintList',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            actno: actno,
            opid: opid
        }
    });
}


/** 
 * 取得急診約束紀錄內容+查檢表紀錄清單
 * @param {string} hsp 院區
 * @param {string} crtno 病歷號
 * @param {string} actno 流水號
 * @param {string} sdte 起始日期
 * @param {string} stme 起始時間
 * @param {string} edte 結束日期
 * @param {string} etme 結束時間
 * @param {string} opid 員編
 */
export function getOprTraintCheckContent(hsp, crtno, actno, sdte, stme, edte, etme, opid)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nisp1000api/Get_ERNUR_TraintCheck',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            actno: actno,
            sdte: sdte,
            stme: stme,
            edte: edte,
            etme: etme,
            opid: opid
        }
    });
}


/**
 * 取得約束查檢表填寫內容
 * @param {string} hsp 院區
 * @param {string} crtno 病歷號
 * @param {string} actno 流水號
 * @param {string} sdte 起始日期
 * @param {string} stme 起始時間
 * @param {string} evldte 填寫日期
 * @param {string} evltme 填寫時間
 * @param {string} opid 員編
 */
export function getOprTraintChkInfo(hsp, crtno, actno, sdte, stme, evldte, evltme, opid)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nisp1000api/Get_ERNUR_TraintCheckInfo',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            actno: actno,
            sdte: sdte,
            stme: stme,
            evldte: evldte,
            evltme: evltme,
            opid: opid
        }
    });
}


/**
 * 取得急診意識評估單
 * @param {string} hsp 院區
 * @param {string} crtno 病歷號
 * @param {string} actno 流水號
 */
export function getOprGcsRec(hsp, crtno, actno)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nisp1000api/Get_ERNurGCSRec',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            actno: actno
        }
    });
}


/**
 * 取得營養照顧記錄清單
 * @param {string} crtno 病歷號
 */
export function getNtrRecList(crtno)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/ntr001api/GetNtrRecList',
        method: 'get',
        params: {
            crtno: crtno
        }
    });
}


/**
 * 取得營養照顧詳細記錄
 * @param {string} crtno 病歷號
 * @param {string} vsdte 訪視日期
 * @param {string} opid  員編
 */
export function getNtrRecDetail(crtno, vsdte, opid)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/ntr001api/Get_NtrRec',
        method: 'get',
        params: {
            crtno: crtno,
            vsdte: vsdte,
            opid: opid
        }
    });
}


/**
 * 取得急診轉歸動向
 * @param {string} organ 院區
 * @param {string} crtno 病歷號
 * @param {string} actno 流水號
 */
export function getOprTransfer(area, crtno, actno) 
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nisp1000api/Get_ERNUR_Transfer',
        method: 'get',
        params: {
            area: area,
            crtno: crtno,
            actno: actno
        }
    });
}


/**
 * 取得急診生命徵象
 * @param {string} organ 院區
 * @param {string} crtno 病歷號
 * @param {string} actno 流水號
 */
export function getOprNurLiveRec(organ, crtno, actno)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/nisp1000api/Get_ERNurLiveRec',
        method: 'get',
        params: {
            organ,
            crtno,
            actno
        }
    });
}

/**
 * 取得麻醉前評估單
 * @param {string} hspnme  院區
 * @param {string} opcrtno 病歷號
 * @param {string} opdate  開刀日期
 * @param {string} scrno   序號
 */
export function getOpr2100Rec(hspnme, opcrtno, opdate, scrno)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/Opr2100api/Get_OPR2100_Rec',
        method: 'get',
        params: {
            hspnme,
            opcrtno,
            opdate,
            scrno
        }
    });
}

/**
 * 術後轉 ICU 交班單
 * @param { string } hspnme  院區
 * @param { string } opcrtno 病歷號
 * @param { string } opdate  開刀日期
 * @param { string } scrno   序號
 */
export function getOpr210Rec(hspnme, opcrtno, opdate, scrno)
{
    return request({
        url: 'http://test.com.tw/OPR/OPRW211/api/Opr012API/Get_OPR012_Rec',
        method: 'get',
        params: {
            hspnme,
            opcrtno,
            opdate,
            scrno
        }
    });
}